# This tool works for Super smash Bros CMC+ v7.

What it can do:
- Install a character
- Update a character
- Check if character already exists and shoe the fighter ID
- Uninstall Character


# Instructions:

CMC Path: Select the folder your cmc+ v7.exe is located

Prepare Character:
- Download character
- Unpack the file
- Make sure all character files are in one folder:
- My_Character
 -- data
 -- fighter
 -- gfx
-- .....

Character Path: Select "My_Character" folder

It does not do any harm if there are any unneeded files like install instructions in the character folder.
All files will copied over to your CMC folder.
As long as those files don't overwrite other files, they won't do any harm.
