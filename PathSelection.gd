tool
extends Control

signal requesting_file_dialog (requester, mode)

onready var _file_dialog_button : Button = $Container/FileDialogButton
onready var _path : TextEdit = $Container/Path
onready var _label : RichTextLabel = $Container/TextLabelContainer/RichTextLabel

export var _label_text : String = "Path"

func _ready() -> void:
	_file_dialog_button.connect("File_Dialog_Button_Pressed", self, "_show_file_dialog")
	_label.text = _label_text


func _show_file_dialog(mode) -> void:
	emit_signal("requesting_file_dialog", self, mode)


func _on_file_dialog_folder_selected(path: String):
	_path.text = path


func get_selected_path() -> String:
	return _path.text
