extends Control


onready var _file_dialog : FileDialog = $FileDialog
onready var _cmc_path_picker : Control = $CMCPath
onready var _character_path_picker : Control = $CharacterPath
onready var _accept_dialog : AcceptDialog = $AcceptDialog
onready var _overwrite_with_zeros_checkbox : CheckBox = $OverwriteZero

var _fighters_path_relative = "/data/fighters.txt"
var _css_path_relative = "/data/css.txt"

var _fighter_id_offset := 65

var file_list : Array

var _file_dialog_requester

var _cmc_path: String
var _character_path: String


func _ready() -> void:
	_cmc_path_picker.connect("requesting_file_dialog", self, "_show_file_dialog")
	_character_path_picker.connect("requesting_file_dialog", self, "_show_file_dialog")
	
	_file_dialog.connect("dir_selected", self, "_on_file_dialog_folder_selected")


func _show_file_dialog(requester, mode) -> void:
	_file_dialog_requester = requester
	_file_dialog.mode = mode
	_file_dialog.popup()


func _on_file_dialog_folder_selected(path: String):
	_file_dialog_requester._on_file_dialog_folder_selected(path)


func _on_AddCharacterButton_button_up() -> void:
	_cmc_path = _cmc_path_picker.get_selected_path()
	_character_path = _character_path_picker.get_selected_path()
	
	if _get_fighter_id() > 0:
		_accept_dialog.dialog_text = "WARN:\nCharacter already exists in fighters.txt.\nCan't install.\nUse the update button if you want to replace or update it."
		_accept_dialog.popup()
		return
	
	print ("_character_path: ", _character_path)
	print ("_cmc_path: ", _cmc_path)
	_copy_files(_character_path, _cmc_path)
	
	var _new_fighter_id = _add_character_to_fighters_txt()
	
	_add_character_to_css(_new_fighter_id)
	
	var _new_fighter_name = _extract_fighter_name()
	
	_accept_dialog.dialog_text = "Install completed!\nName: " + _new_fighter_name + "\nID: " + str(_new_fighter_id)
	_accept_dialog.popup()


func _on_CheckUpdateButton_button_up() -> void:
	_cmc_path = _cmc_path_picker.get_selected_path()
	_character_path = _character_path_picker.get_selected_path()
	
	var _dialog_text = ""
	if _get_fighter_id() > 0:
		_dialog_text += "Character found: " + _extract_fighter_name()
		_dialog_text += "\nID: " + str(_get_fighter_id())
		_dialog_text += ".\nUse the update button if you want to update the character."
		_dialog_text += ".\nUse the delete button if you want to delete the character."
	else:
		_dialog_text += "Character not found: " + _extract_fighter_name()
		_dialog_text += ".\nUse the install button if you want to install the character."
	
	_accept_dialog.dialog_text = _dialog_text
	_accept_dialog.popup()


func _on_UpdateCharacter_button_up() -> void:
	_cmc_path = _cmc_path_picker.get_selected_path()
	_character_path = _character_path_picker.get_selected_path()
	
	if _get_fighter_id() < 0:
		_accept_dialog.dialog_text = "WARN:\n" + _extract_fighter_name() + "  does not exist fighters.txt.\nCan't update.\nUse the install button instead."
		_accept_dialog.popup()
		return
	
	_copy_files(_character_path, _cmc_path)
	
	_accept_dialog.dialog_text = "Update successful"
	_accept_dialog.popup()


func _on_UninstallButton_button_up() -> void:
	_cmc_path = _cmc_path_picker.get_selected_path()
	_character_path = _character_path_picker.get_selected_path()
	
	var _old_fighter_id = _get_fighter_id()
	
	if _get_fighter_id() < 0:
		_accept_dialog.dialog_text = "Can not uninstall. Character does not exist in fighter.txt"
		_accept_dialog.popup()
		return
	
	_remove_character_from_files()
	_delete_character_files()
	
	var _dialog_text: String = ""
	
	_dialog_text += "Character removed: " + _extract_fighter_name()
	_dialog_text += "\nOld ID: " + str(_old_fighter_id)
	
	_accept_dialog.dialog_text = _dialog_text
	_accept_dialog.popup()


# Removes the character from fighter.txt and css.txt
func _remove_character_from_files() -> void:
	var _fighter_name = _extract_fighter_name()
	var _fighter_id = _get_fighter_id()
	var _fighters_txt = _read_fighters_txt()
	var _css_txt = _read_css_txt()
	
	# Delete from fighters.txt
	_fighters_txt.erase(_fighter_name)
	_fighters_txt[0] = str(int(_fighters_txt[0]) -1)
	
	# Remove from css.txt
	var _line_character = 0
	var _col_character = 0
	for line in range(0, _css_txt.size()):
		for column in range(0, _css_txt[line].size()):
			if _fighter_id == int(_css_txt[line][column]):
				_line_character = line
				_col_character = column
				break
	
	# Update make the character ID in css.txt to 0000
	var overwrite_with_zero = _overwrite_with_zeros_checkbox.pressed
	if overwrite_with_zero:
		_css_txt[_line_character][_col_character] = "0000"
	else:
		#Delete the cell with the fighter id from the line it is in
		_css_txt[_line_character].remove(_col_character)
		
		# Fill the gap
		# Take the first entry of the next line and add it to the line the fighter was removed
		for line_id in range(_line_character, _css_txt.size() -1):
			_css_txt[line_id].append(_css_txt[line_id+1][0])
			_css_txt[line_id+1].remove(0)
	
	# Decrease all IDs which are greater than the character IDs by 1
	for line in range(0, _css_txt.size()):
		for column in range(0, _css_txt[line].size()):
			if (int(_css_txt[line][column]) > _fighter_id) && (_css_txt[line][column] != "9999"):
				var tmp_id = int(_css_txt[line][column]) -1
				_css_txt[line][column] = str(tmp_id).pad_zeros(4)
	
	# Remove last line if empty
	if _css_txt.back().empty():
		_css_txt.pop_back()
	
	_write_fighters_txt_to_file(_fighters_txt)
	_write_css_txt_to_file(_css_txt)


# Deletes files which are save to delete
func _delete_character_files() -> void:
	_build_file_list(_character_path)
	var _fighter_name = _extract_fighter_name()
	
	var dir = Directory.new()
	
	for file_suffix in file_list:
		if file_suffix.ends_with(".dat") or file_suffix.ends_with(".bin") or file_suffix.find("/" + _fighter_name) != -1:
			var file_path = _cmc_path + file_suffix
			print ("file_path: ", file_path)
			dir.remove(file_path)
			


func _extract_fighter_name() -> String:
	
	var _fighter_name = ""
	
	var _fighter_folder = _character_path + "/" + "fighter"
	var dir = Directory.new()
	var error = dir.open(_fighter_folder)
	if error != OK:
		print("Error opening: " + _fighter_folder)
		return _fighter_name
	
	dir.list_dir_begin(true, true)
	
	var _fighter_bin_name: String
	
	while true:
		_fighter_bin_name = dir.get_next()
		if _fighter_bin_name.ends_with(".bin"):
			_fighter_bin_name = _fighter_bin_name.trim_suffix(".bin")
			break
		elif _fighter_bin_name == "":
			break
	
	return _fighter_bin_name


func _get_fighter_id() -> int:
	var file_in = File.new()
	
	var error = file_in.open(_cmc_path + _fighters_path_relative, File.READ)
	if error != OK:
		print("ERROR opening fighter.txt")
		return -2
	
	var _fighter_bin_name = _extract_fighter_name()
	
	var _fighter_id := 0
	
	while not file_in.eof_reached():
		var _line = file_in.get_line()
		
		if _line == _fighter_bin_name:
			return _fighter_id + _fighter_id_offset
		
		_fighter_id += 1
	
	file_in.close()
	
	return -1


# returns the ID of the newly added fighter
func _add_character_to_fighters_txt() -> int:
	
	var _fighters_txt : Array = _read_fighters_txt()
	
	# increase number count
	_fighters_txt[0] = str(int(_fighters_txt[0]) +1)
	
	# Get the name of the fighter.bin file
	var _fighter_bin_name = _extract_fighter_name()
	
	if _fighter_bin_name == "":
		print("ERROR: No fighter .bin file")
		return -1
	
	# Append to array
	_fighters_txt.append(_fighter_bin_name)
	
	_write_fighters_txt_to_file(_fighters_txt)
	
	return _fighters_txt.size()-1 + _fighter_id_offset


func _read_fighters_txt() -> Array:
	var _fighters_txt : Array
	
	var file_in = File.new()
	
	var error = file_in.open(_cmc_path + _fighters_path_relative, File.READ)
	if error != OK:
		print("ERROR opening fighter.txt")
		return []
		
	while not file_in.eof_reached():
		var _line = file_in.get_line()
		_fighters_txt.append(_line)
	
	file_in.close()
	
	return _fighters_txt

func _write_fighters_txt_to_file(_fighters_txt: Array) -> void:
	# Write array to file
	# We don't want to add an empty line after the last line
	# That is why we don't use "store_line", but add the new line manually
	var fighters_txt_string: String = ""
	var line_count = 0
	for line in _fighters_txt:
		fighters_txt_string += line
		if line_count != _fighters_txt.size()-1:
			fighters_txt_string += "\r\n"
		
		line_count += 1
	
	var dir = Directory.new()
	var error = dir.rename(_cmc_path + _fighters_path_relative, _cmc_path + "/data/fighters_backup.txt")
	if error:
		print("Error backing up fighters:txt")
	
	var file_out = File.new()
	file_out.open(_cmc_path + _fighters_path_relative, File.WRITE)
	file_out.store_string(fighters_txt_string)
	file_out.close()


func _add_character_to_css(_fighter_id: int) -> void:
	var _css_txt : Array = _read_css_txt()
	
	var _num_lines = _css_txt.size()
	var _num_cols = 0
	for line in _css_txt:
		_num_cols = max (_num_cols, line.size())
	
	# Append one line, if the last row is full already
	if _css_txt[(_css_txt.size()-1)].size() == _num_cols:
		var a: Array
		_css_txt.append(a)
	
	# Format to 0xxx and append to last line
	var _fighter_id_formatted_for_css = str(_fighter_id).pad_zeros(4)
	_css_txt[(_css_txt.size()-1)].append(_fighter_id_formatted_for_css)
	
	_write_css_txt_to_file(_css_txt)


func _read_css_txt() -> Array:
	var _css_txt: Array = []
	
	var file = File.new()
	
	var error = file.open(_cmc_path + _css_path_relative, File.READ)
	if error != OK:
		print("ERROR opening css.txt")
		return _css_txt
	
	
	while not file.eof_reached():
		var _line = file.get_line()
		_css_txt.append(Array(_line.split(" ", false)))
	
	file.close()
	
	return _css_txt


func _write_css_txt_to_file(_css_txt: Array) -> void:
	# Write array to file
	# We don't want to add an empty line after the last line
	# That is why we don't use "store_line", but add the new line manually
	var line_count = 0
	var css_txt_string: String = ""
	for line in _css_txt:
		var i = 0
		for cell in line:
			i += 1
			css_txt_string += cell
			if i != line.size():
				css_txt_string += " "
		if line_count != _css_txt.size()-1:
			css_txt_string += "\r\n"
		
		line_count += 1
	
	css_txt_string += " "
	
	var dir = Directory.new()
	var error = dir.rename(_cmc_path + _css_path_relative, _cmc_path + "/data/css_backup.txt")
	if error:
		print("Error backing up fighters:txt")
	
	var file = File.new()
	file.open(_cmc_path + _css_path_relative, File.WRITE)
	file.store_string(css_txt_string)
	file.close()


func _copy_files(source: String, destination: String) -> void:
	var dir = Directory.new()

	if not dir.dir_exists(destination):
		dir.make_dir_recursive(destination)
	
	var error = dir.open(source)
	if error != OK:
		print("Error copying from: " + source, " to: " + destination)
		return
	
	dir.list_dir_begin(true, true)
	

	var file = dir.get_next()
	while file != "":
		var source_new = source + "/" + file
		var destination_new = destination + "/" + file 
		
		if dir.current_is_dir():
			_copy_files(source_new, destination_new)
		else:
			print("Copy from: ",  source_new, " to: ", destination_new)
			dir.copy(source_new, destination_new)
		
		file = dir.get_next()

	dir.list_dir_end()


func _build_file_list(source: String) -> void:
	var dir = Directory.new()
	
	var error = dir.open(source)
	if error != OK:
		print("Error building file list")
		return
	
	dir.list_dir_begin(true, true)
	

	var file = dir.get_next()
	while file != "":
		var source_new = source + "/" + file
		
		if dir.current_is_dir():
			_build_file_list(source_new)
		else:
			file_list.append(source_new.trim_prefix(_character_path))
		
		file = dir.get_next()

	dir.list_dir_end()
