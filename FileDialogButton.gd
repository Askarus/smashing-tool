extends Button

class_name FileDialogButton

signal File_Dialog_Button_Pressed (mode)

enum _dialog_mode {
	OPEN_FILE,
	OPEN_FILES,
	OPEN_DIR
	OPEN_ANY
	SAVE_FILE
	}
export(_dialog_mode) var _dialog_mode_selected

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


func _on_FileDialogButton_button_up() -> void:
	emit_signal("File_Dialog_Button_Pressed", _dialog_mode_selected)
